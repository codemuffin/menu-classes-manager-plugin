jQuery(document).ready(function($){

	/*
	 * -----------------------
	 *  BOOTSTRAP
	 * -----------------------
	 */

	$('[data-toggle="popover"]').popover();



	/*
	 * -----------------------
	 *  PANEL SWITCHING
	 * -----------------------
	 */


	// Get 'remember tab?' setting. Make one if it's not set
	remember_tab_setting = $('input[name="muffin_options[remember_tab]"]:checked').val();
	if(typeof remember_tab_setting === 'undefined') {
		remember_tab_setting = 'refresh-and-save';
	}

	// Delete tab saving cookie if the setting is 'neither;
	if( remember_tab_setting == 'neither' ) {
		Cookies.remove('muffintab');
	};

	// Rememeber last panel
	if ( remember_tab_setting !== 'neither' ) {
		if( remember_tab_setting == 'refresh-and-save' || (remember_tab_setting == 'save-only' && $('#setting-error-settings_updated').length) ) {
			if( Cookies.get('muffintab') ) {active_panel_hash = Cookies.get('muffintab');}
			else {active_panel_hash = '#main';}

			if( $('.muffin-nav a[href="'+active_panel_hash+'"]').length == 0 ) {active_panel_hash = '#main';} // This can be improved!

			active_panel = active_panel_hash.replace('#','panel-');		

			$('.settings-panel').removeClass('current');
			$('.muffin-nav li').removeClass('active');

			$('#'+active_panel).addClass('current');
			$('.muffin-nav a[href="'+active_panel_hash+'"]').parent('li').addClass('active');
		}
	}

	// Switch tab panels
	$('.muffin-nav a').mousedown(function(e) {
		active_panel_hash = $(this).attr('href');
		active_panel = active_panel_hash.replace('#','panel-');

		$('.settings-panel').removeClass('current');
		$('.muffin-nav li').removeClass('active');

		$('#'+active_panel).addClass('current');
		$('.muffin-nav a[href="'+active_panel_hash+'"]').parent('li').addClass('active');

		if( remember_tab_setting !== 'neither' ) {
			Cookies.set('muffintab', active_panel_hash);
		}
	});

});