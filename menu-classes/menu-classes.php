<?php 
/**
 * Plugin Name: Menu Classes Manager
 * Plugin URI: http://codemuffin.com
 * Description: Add, remove and edit the core menu classes, and update wp_page_menu classes
 * Version: 1
 * Author: Chris Bloomfield
 * Author URI: http://codemuffin.com
 * License: GPL2
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

add_action( 'admin_menu', 'muffin_menus_add_admin_menu' ); // Add admin menu
add_action( 'admin_init', 'muffin_menus_options_init' ); // Initialise options page
add_action( 'admin_enqueue_scripts', 'muffin_menus_enqueue_admin' ); // Enqueue options CSS and JS

include('filters.php'); // Contains all the menu class filters

// Admin menu
function muffin_menus_add_admin_menu() { 
	add_object_page(
		'Manage Menu Classes', // Page <title>
		'Menu Classes', // Menu item name
		'manage_options', // Required user capabilities
		'muffin-menus', // Slug (URL part)
		'muffin_menu_form', // Callback function (displays the page)
		'dashicons-menu' // Dashicon
	);
}

// Enqueue admin styles & scripts
function muffin_menus_enqueue_admin($hook) {
	if ( $hook != 'toplevel_page_muffin-menus' ) return; // Only enqueue these files on the actual settings page

	// Enqueue required styles
	wp_enqueue_style( 'fontawesome', 			plugin_dir_url( __FILE__ ) . '/admin/css/font-awesome/css/font-awesome.min.css', '', '4.3.0' );
	wp_enqueue_style( 'muffin-admin-bootstrap', plugin_dir_url( __FILE__ ) . '/admin/css/bootstrap.min.css', '', '3.0.3' );
	wp_enqueue_style( 'muffin-admin-style', 	plugin_dir_url( __FILE__ ) . '/admin/css/admin.css', array('fontawesome', 'muffin-admin-bootstrap'), '1.0' );

	// Enqueue required scripts
	wp_enqueue_script( 'muffin-admin-bootstrap',plugin_dir_url( __FILE__ ) . '/admin/js/bootstrap.min.js', array('jquery'), '3.3.4' );
	wp_enqueue_script( 'js-cookie', 			plugin_dir_url( __FILE__ ) . '/admin/js/js.cookie.js', array('jquery'), '2.0.0-pre' );
	wp_enqueue_script( 'muffin-admin-settings',	plugin_dir_url( __FILE__ ) . '/admin/js/admin.js', array('jquery', 'js-cookie', 'muffin-admin-bootstrap') );
}

// Plugin options: Register the option (setting), set up the defaults, and setup the settings sections
function muffin_menus_options_init() {
	register_setting( 'muffin_menus', 'muffin_menus' ); // Register the option

	muffin_menus_defaults(); // Set the default options
	
	// Add sections for each options tab. This helps keep everything nicely separated
	add_settings_section('muffin_menus_general', '', 'muffin_menus_general_cb',  'muffin_menus_general');
	add_settings_section('muffin_menus_old',     '', 'muffin_menus_old_cb',      'muffin_menus_old');
	add_settings_section('muffin_menus_replace', '', 'muffin_menus_replace_cb',  'muffin_menus_replace');
	add_settings_section('muffin_menus_info',    '', 'muffin_menus_info_cb',     'muffin_menus_info');
	add_settings_section('muffin_menus_data',    '', 'muffin_menus_data_cb',     'muffin_menus_data');	
}

// Default options. Applied on first load
function muffin_menus_defaults() {

	$options = get_option('muffin_menus');
	
	$options_default['menu_enable_active_class'] 		= 'enabled';
	$options_default['menu_enable_parent_class'] 		= 'enabled';
	$options_default['menu_enable_first_class'] 		= 'enabled';
	$options_default['menu_enable_last_class'] 			= 'enabled';

	$options_default['menu_enable_depth_class_top'] 	= 'enabled';
	$options_default['menu_enable_depth_class_below'] 	= 'enabled';	
	$options_default['menu_enable_depth_classes']		= 'enabled';

	$options_default['menu_active_class'] 				= 'menu-active';
	$options_default['menu_parent_class']		 		= 'menu-parent';
	$options_default['menu_first_class']		 		= 'menu-first';
	$options_default['menu_last_class']		 			= 'menu-last';

	$options_default['menu_depth_class_top'] 			= 'menu-top';
	$options_default['menu_depth_class_below'] 			= 'sub-menu-item';
	$options_default['menu_depth_class_prefix'] 		= 'depth';	

	$options_default['menu_submenu_class'] 				= 'sub-menu';

	$options_default['menu_enable_remove_ids']			= 'disabled';
	$options_default['menu_enable_remove_classes']		= 'disabled';

	$options_default['preserve_page_item'] 				= 'disabled';
	$options_default['preserve_page_item_has_children'] = 'disabled';
	$options_default['preserve_current_page_item'] 		= 'disabled';
	$options_default['preserve_current_page_parent'] 	= 'disabled';
	$options_default['preserve_current_page_ancestor'] 	= 'disabled';

	$options_default['preserve_menu_item'] 				= 'disabled';
	$options_default['preserve_menu_item_home'] 		= 'disabled';	
	$options_default['preserve_menu_item_has_children']	= 'disabled';
	$options_default['preserve_current_menu_item'] 		= 'disabled';	
	$options_default['preserve_current_menu_parent'] 	= 'disabled';
	$options_default['preserve_current_menu_ancestor'] 	= 'disabled';

	$options_default['preserve_menu_active'] 			= 'enabled';
	$options_default['preserve_menu_parent'] 			= 'enabled';
	$options_default['preserve_submenu'] 				= 'enabled';
	// $options_default['preserve_depth'] 				= 'enabled'; 	# How to use?

	$options_default['menu_update_wp_page_menu'] 		= 'enabled';
	$options_default['menu_fallback_menus_only'] 		= 'enabled';
	$options_default['menu_preserve_old_classes'] 		= 'enabled';
	$options_default['menu_preserve_old_menu_class'] 	= 'disabled';

	/*
	The code below allows for further plugin development and testing, and lists snippets I use for plugin development.
	They all serve different purposes, and may help you in your own plugin development.
	*/
	
	# [1] Add any new default data, but don't overwrite anything.
	# [?] Use to add new data while preserving all user changes
	// $options = ( isset($options) && is_array($options) ? array_merge($options_default,$options) : $options_default );
	// update_option('muffin_menus', $options);

	# [2] Restore defaults, but KEEP all other data
	# [?] Use to both add new data, and overwrite user changes to default data. Good with a 'Restore Defaults' button
	// $options = ( isset($options) && is_array($options) ? array_merge($options,$options_default,) : $options_default );
	// update_option('muffin_menus', $options);

	# [3] Restore defaults, and WIPE all other data
	# [?] Use to both replace all option data with defaults, and clear all user data. Use to emulate fresh installs
	update_option('muffin_menus', $options_default);

	# [4] Delete the option entirely
	# [?] Use to remove the option from the database entirely - good for resets, testing and cleanup
	// delete_option('muffin_menus');

}

// The options page output
function muffin_menu_form() { ?>

	<?php
	/*
		Shorthand function for "if X variable is set, return the value of that variable; otherwise,
		either return false, or if a second argument is passed, return the value of that argument".
		It is similar in usage to WordPress' own get_option function.
	*/
		function issetor(&$var, $default=false) {
			return isset($var) ? $var : $default;
		}
	?>

	<div class="muffin-header">
		<div class="container">
			<h1>Muffin Menu Options</h1>
			<?php settings_errors(); // Show the 'settings updated' message, or any error messages ?>
		</div>
	</div>

	<div class="muffin-nav">
		<nav class="navbar navbar-default">
			<div class="container">
				<ul class="nav navbar-nav">
					<li class="active"><a href="#main"><i class="fa fa-fw fa-2x fa-plus"></i><br />Add</a></li>
					<li><a href="#replace"><i class="fa fa-fw fa-2x fa-minus"></i><br />Remove</a></li>
					<li><a href="#oldmenus"><i class="fa fa-fw fa-2x fa-bolt"></i><br />Update</a></li>
					<li><a href="#info"><i class="fa fa-fw fa-2x fa-info"></i><br />Info</a></li>
					<li><a href="#data"><i class="fa fa-fw fa-2x fa-database"></i><br />Data</a></li>
				</ul>
			</div>
		</nav>
	</div>

	<form action='options.php' method='post' class="options-form">
		<div class="container">

		<?php settings_fields( 'muffin_menus' ); // Echo the code needed to save options ?>

		<div class="settings-panel current" id="panel-main"><?php do_settings_sections( 'muffin_menus_general' ) ?></div>
		<div class="settings-panel" id="panel-oldmenus"><?php do_settings_sections( 'muffin_menus_old' ); ?></div>
		<div class="settings-panel" id="panel-replace"><?php do_settings_sections( 'muffin_menus_replace' ); ?></div>
		<div class="settings-panel" id="panel-data"><?php do_settings_sections( 'muffin_menus_data' ); ?></div>
		<div class="settings-panel" id="panel-info"><?php do_settings_sections( 'muffin_menus_info' ); ?></div>		

		<?php submit_button(); ?>
		
		</div>
	</form>

<?php }


function muffin_menus_general_cb() { $options = get_option('muffin_menus'); ?>

	<div class="row row-title">
		<div class="col-sm-10 col-title"><h3>Add &amp; Edit Classes</h3></div>
	</div>

	<div class="row row-info">
		<div class="col-sm-10 col-info"><p>Add your own custom menu classes. Applies to all menu types, including menu widgets.</p></div>
	</div>

	<div class="row row-setting row-alpha">
		<div class="col-sm-3 col-lg-2 col-name">General</div>
		<div class="col-sm-9 col-lg-7 col-setting">
			<label>
				<input type="checkbox" name="muffin_menus[menu_enable_active_class]" <?php checked( issetor($options['menu_enable_active_class']), 'enabled' ); ?> value="enabled">
				Apply <code>menu-active</code> to active menu items
			</label><br />
			<label>
				<input type="checkbox" name="muffin_menus[menu_enable_parent_class]" <?php checked( issetor($options['menu_enable_parent_class']), 'enabled' ); ?> value="enabled">
				Apply <code>menu-parent</code> to menu items with children
			</label><br />
			<label>
				<input type="checkbox" name="muffin_menus[menu_enable_first_class]" <?php checked( issetor($options['menu_enable_first_class']), 'enabled' ); ?> value="enabled">
				Apply <code>menu-first</code> to the first menu item
			</label><br />
			<label>
				<input type="checkbox" name="muffin_menus[menu_enable_last_class]" <?php checked( issetor($options['menu_enable_last_class']), 'enabled' ); ?> value="enabled">
				Apply <code>menu-last</code> to the last menu item
			</label>
		</div>
	</div>

	<div class="row row-setting row">
		<div class="col-sm-3 col-lg-2 col-name col-empty">Depth</div>
		<div class="col-sm-9 col-lg-7 col-setting">
			<label>
				<input type="checkbox" name="muffin_menus[menu_enable_depth_class_top]" <?php checked( issetor($options['menu_enable_depth_class_top']), 'enabled' ); ?> value="enabled">
				Apply <code>menu-top</code> to all top-level menu items <small>(regardless of if they are parents)</small>
			</label><br />
			<label>
				<input type="checkbox" name="muffin_menus[menu_enable_depth_class_below]" <?php checked( issetor($options['menu_enable_depth_class_below']), 'enabled' ); ?> value="enabled">
				Apply <code>sub-menu-item</code> to all menu items within sub-menus
			</label><br />
			<label>
				<input type="checkbox" name="muffin_menus[menu_enable_depth_classes]" <?php checked( issetor($options['menu_enable_depth_classes']), 'enabled' ); ?> value="enabled">
				Apply <code>depth-*</code> classes <small>(starting with <code>depth-0</code> for top-level menu items)</small>
			</label>
		</div>
	</div>

	<div class="row row-setting row-omega">
		<div class="col-sm-3 col-lg-2 col-name">Edit Custom Classes</div>
		<div class="col-sm-9 col-lg-7 col-setting">
			<label>
				<div class="pretext">Active:</div>
				<input type="text" id="f-img-url" name="muffin_menus[menu_active_class]" value="<?php echo issetor($options['menu_active_class']); ?>" />
				&nbsp; <span class="note">Default: <code>menu-active</code></span>
			</label><br />
			<label>
				<div class="pretext">Parent:</div>
				<input type="text" name="muffin_menus[menu_parent_class]" value="<?php echo issetor($options['menu_parent_class']); ?>" />
				&nbsp; <span class="note">Default: <code>menu-parent</code></span>
			</label><br />
			<label>
				<div class="pretext">First:</div>
				<input type="text" name="muffin_menus[menu_first_class]" value="<?php echo issetor($options['menu_first_class']); ?>" />
				&nbsp; <span class="note">Default: <code>menu-first</code></span>
			</label><br />
			<label>
				<div class="pretext">Last:</div>
				<input type="text" name="muffin_menus[menu_last_class]" value="<?php echo issetor($options['menu_last_class']); ?>" />
				&nbsp; <span class="note">Default: <code>menu-last</code></span>
			</label><br />
			<label>
				<div class="pretext">Top Level:</div>
				<input type="text" name="muffin_menus[menu_depth_class_top]" value="<?php echo issetor($options['menu_depth_class_top']); ?>" />
				&nbsp; <span class="note">Default: <code>menu-top</code></span>
			</label><br />
			<label>
				<div class="pretext">Child Level:</div>
				<input type="text" name="muffin_menus[menu_depth_class_below]" value="<?php echo issetor($options['menu_depth_class_below']); ?>" />
				&nbsp; <span class="note">Default: <code>sub-menu-item</code></span>
			</label><br />
			<label>
				<div class="pretext">Depth Prefix:</div>
				<input type="text" name="muffin_menus[menu_depth_class_prefix]" value="<?php echo issetor($options['menu_depth_class_prefix']); ?>" />
				&nbsp; <span class="note">Default: <code>depth</code></span>
			</label>
		</div>
	</div>

	<div class="row row-setting row-omega">
		<div class="col-sm-3 col-lg-2 col-name">Edit Inbuilt Classes</div>
		<div class="col-sm-9 col-lg-7 col-setting">
			<label>
				<div class="pretext">Sub-Menu:</div>
				<input type="text" name="muffin_menus[menu_submenu_class]" value="<?php echo issetor($options['menu_submenu_class']); ?>" />
				&nbsp; <span class="note">Default: <code>sub-menu</code></span>
			</label>
		</div>
	</div>

<?php }


function muffin_menus_old_cb() { $options = get_option('muffin_menus'); ?>

	<div class="row row-title">
		<div class="col-sm-10 col-title"><h3>Modernise Old Menus <small>(wp_page_menu)</small></h3></div>
	</div>

	<div class="row row-info">
		<div class="col-sm-10 col-info">
			<p>
			Older versions of WordPress generated menus with the function <a href="https://codex.wordpress.org/Function_Reference/wp_page_menu" target="_blank">wp_page_menu</a>.
			Modern versions of WP use the function <a href="https://codex.wordpress.org/Function_Reference/wp_page_menu" target="_blank">wp_page_menu</a> instead,
			but the former is still used sometimes, like when a theme says that a custom menu should be used,
			but no menu has been set yet in the <a href="<?php echo get_admin_url() . 'nav-menus.php'; ?>">Menu settings</a>.

			This can be problematic because the older function doesn't include lots of classes (like <code>menu-item</code>) that the modern function does.
			These settings allow you to add those classes.
			</p>
		</div>
	</div>

	<div class="row row-setting row-alpha">
		<div class="col-sm-3 col-lg-2 col-name">Add Classes</div>
		<div class="col-sm-9 col-lg-7 col-setting">
			<label>
				<input type="checkbox" name="muffin_menus[menu_update_wp_page_menu]" <?php checked( issetor($options['menu_update_wp_page_menu']), 'enabled' ); ?> value="enabled">
				Add modern classes to old menus
			</label><br />
			<p class="description">
				If checked, the classes WordPress uses for menus created with <a href="https://codex.wordpress.org/Function_Reference/wp_nav_menu" target="_blank">wp_nav_menu</a>
				will be applied to <a href="https://codex.wordpress.org/Function_Reference/wp_page_menu" target="_blank">wp_page_menu</a> menus too.
				This means your styles will be preserved if the menu becomes unset.
			</p>
		</div>
	</div>

	<div class="row row-setting">
		<div class="col-sm-3 col-lg-2 col-name">Retain Classes</div>
		<div class="col-sm-9 col-lg-7 col-setting">
			<label>
				<input type="checkbox" name="muffin_menus[menu_preserve_old_classes]" <?php checked( issetor($options['menu_preserve_old_classes']), 'enabled' ); ?> value="enabled">
				Keep the old menu item classes
			</label>
			<p class="description">
				Uncheck to remove the old menu classes from menus generated with <a href="https://codex.wordpress.org/Function_Reference/wp_page_menu" target="_blank">wp_page_menu</a>.
				It's recommended that you keep this checked, as your theme or installed plugins may depend on these classes.
			</p>
		</div>
	</div>

	<div class="row row-setting">
		<div class="col-sm-3 col-lg-2 col-name">Fallback Menus Only</div>
		<div class="col-sm-9 col-lg-7 col-setting">
			<label>
				<input type="checkbox" name="muffin_menus[menu_fallback_menus_only]" <?php checked( issetor($options['menu_fallback_menus_only']), 'enabled' ); ?> value="enabled">
				Only apply settings to fallback menus
			</label>
			<p class="description">
				If you aren't keeping the old menu classes, it's recommended that you only do so with fallback menus
				(menus generated by the older <a href="https://codex.wordpress.org/Function_Reference/wp_page_menu" target="_blank">wp_page_menu</a>
				function when a custom menu has not been set). Otherwise, you may disrupt the functionality of your theme or installed plugins.

				Note that this checks for a setting of <code>'theme_location' => 'menu-location'</code> so - this won't work if you're just using <code>'menu' => 'xxx'</code> on its own.
			</p>
		</div>
	</div>

	<div class="row row-setting row-omega">
		<div class="col-sm-3 col-lg-2 col-name">Wrapper Class</div>
		<div class="col-sm-9 col-lg-7 col-setting">
			<label>
				<input type="checkbox" name="muffin_menus[menu_preserve_old_menu_class]" <?php checked( issetor($options['menu_preserve_old_menu_class']), 'enabled' ); ?> value="enabled">
				Keep a class of 'menu' on the wrapping DIV
			</label>
			<p class="description">
				Old menus use the class <code>menu</code> on the containing DIV, while modern menus use the class on a UL.
				Check this box to leave the <code>menu</code> class on the DIV.
				It will still be added to the UL though, as part of the menu modernisation, so only check this if you need DIV class preserved for some reason.
			</p>
		</div>
	</div>

<?php }


function muffin_menus_replace_cb() { $options = get_option('muffin_menus'); ?>

	<div class="row row-title">
		<div class="col-sm-10 col-title"><h3>Remove IDs &amp; Classes</h3></div>
	</div>

	<div class="row row-info">
		<div class="col-sm-10 col-info"><p>Use these settings to clean up your WP menus. Use with care, as your theme and installed plugins may rely on classes or IDs to function.</p></div>
	</div>

	<div class="row row-setting row-alpha">
		<div class="col-sm-3 col-lg-2 col-name">Remove IDs &amp; Classes</div>
		<div class="col-sm-9 col-lg-7 col-setting">
			<label>
				<input type="checkbox" name="muffin_menus[menu_enable_remove_ids]" <?php checked( issetor($options['menu_enable_remove_ids']), 'enabled' ); ?> value="enabled">
				Remove all IDs
			</label>
			<p class="description">
				Custom menu items have IDs set, like 'menu-item-1046'.
				This option disables them, and affects any menus generated with <code>wp_nav_menu</code>.
				This includes menu widgets, but excludes menus created with <code>wp_page_menu</code>.
			</p>
		</div>
	</div>

	<div class="row row-setting">
		<div class="col-sm-3 col-lg-2 col-name col-empty"></div>
		<div class="col-sm-9 col-lg-7 col-setting">
			<label>
				<input type="checkbox" name="muffin_menus[menu_enable_remove_classes]" <?php checked( issetor($options['menu_enable_remove_classes']), 'enabled' ); ?> value="enabled">
				Remove all menu classes
				<p class="description">
					Allows you to remove ALL classes from all WordPress menu items. You can still keep useful classes with the 'keep classes' options below.
					Note that this won't apply to non-fallback uses of wp_page_menu, if you have the 'Apply to Fallback Menus Only' setting checked.
				</p>
			</label>
		</div>
	</div>

	<div class="row row-setting row-omega">
		<div class="col-sm-3 col-lg-2 col-name">Keep Menu Classes</div>
		<div class="col-sm-9 col-lg-7 col-setting">
			<label>
				<input type="checkbox" name="muffin_menus[preserve_page_item]" <?php checked( issetor($options['preserve_page_item']), 'enabled' ); ?> value="enabled">
				Keep the wp_page_menu class <code>page_item</code>
			</label><br />
			<label>
				<input type="checkbox" name="muffin_menus[preserve_page_item_has_children]" <?php checked( issetor($options['preserve_page_item_has_children']), 'enabled' ); ?> value="enabled">
				Keep the wp_page_menu class <code>page_item_has_children</code>
			</label><br />
			<label>
				<input type="checkbox" name="muffin_menus[preserve_current_page_item]" <?php checked( issetor($options['preserve_current_page_item']), 'enabled' ); ?> value="enabled">
				Keep the wp_page_menu class <code>current_page_item</code>
			</label><br />
			<label>
				<input type="checkbox" name="muffin_menus[preserve_current_page_parent]" <?php checked( issetor($options['preserve_current_page_parent']), 'enabled' ); ?> value="enabled">
				Keep the wp_page_menu class <code>current_page_parent</code>
			</label><br />
			<label>
				<input type="checkbox" name="muffin_menus[preserve_current_page_ancestor]" <?php checked( issetor($options['preserve_current_page_ancestor']), 'enabled' ); ?> value="enabled">
				Keep the wp_page_menu class <code>current_page_ancestor</code>
			</label><br />

			<br />

			<label>
				<input type="checkbox" name="muffin_menus[preserve_menu_item]" <?php checked( issetor($options['preserve_menu_item']), 'enabled' ); ?> value="enabled">
				Keep the wp_nav_menu class <code>menu-item</code>
			</label><br />
			<label>
				<input type="checkbox" name="muffin_menus[preserve_menu_item_has_children]" <?php checked( issetor($options['preserve_menu_item_has_children']), 'enabled' ); ?> value="enabled">
				Keep the wp_nav_menu class <code>menu-item-has-children</code>
			</label><br />
			<label>
				<input type="checkbox" name="muffin_menus[preserve_menu_item_home]" <?php checked( issetor($options['preserve_menu_item_home']), 'enabled' ); ?> value="enabled">
				Keep the wp_nav_menu class <code>menu-item-home</code>
			</label><br />
			<label>
				<input type="checkbox" name="muffin_menus[preserve_current_menu_item]" <?php checked( issetor($options['preserve_current_menu_item']), 'enabled' ); ?> value="enabled">
				Keep the wp_nav_menu class <code>current-menu-item</code>
			</label><br />
			<label>
				<input type="checkbox" name="muffin_menus[preserve_current_menu_parent]" <?php checked( issetor($options['preserve_current_menu_parent']), 'enabled' ); ?> value="enabled">
				Keep the wp_nav_menu class <code>current-menu-parent</code>
			</label><br />
			<label>
				<input type="checkbox" name="muffin_menus[preserve_current_menu_ancestor]" <?php checked( issetor($options['preserve_current_menu_ancestor']), 'enabled' ); ?> value="enabled">
				Keep the wp_nav_menu class <code>current-menu-ancestor</code>
			</label><br />

			<br />

			<label>
				<input type="checkbox" name="muffin_menus[preserve_menu_active]" <?php checked( issetor($options['preserve_menu_active']), 'enabled' ); ?> value="enabled">
				Keep the custom class <code>menu-active</code>
				<span class="note">(currently set to: <code><?php echo $options['menu_active_class']; ?></code>)</span>
			</label><br />
			<label>
				<input type="checkbox" name="muffin_menus[preserve_menu_parent]" <?php checked( issetor($options['preserve_menu_parent']), 'enabled' ); ?> value="enabled">
				Keep the custom class <code>menu-parent</code>
				<span class="note">(currently set to: <code><?php echo $options['menu_parent_class']; ?></code>)</span>
			</label><br />
			<label>
				<input type="checkbox" name="muffin_menus[preserve_submenu]" <?php checked( issetor($options['preserve_submenu']), 'enabled' ); ?> value="enabled">
				Keep the custom classes <code>menu-top</code> &amp; <code>sub-item</code>
				<span class="note">(currently set to: <code><?php echo $options['menu_depth_class_top']; ?></code> &amp; <code><?php echo $options['menu_depth_class_below']; ?></code>)</span>
			</label>
		</div>
	</div>


<?php }


function muffin_menus_info_cb() { $options = get_option('muffin_menus'); ?>

	<div class="row row-title">
		<div class="col-sm-10 col-title"><h3>Information</h3></div>
	</div>

	<div class="row row-info">
		<div class="col-sm-10 col-info"><p>Find out what the functions mentioned in this plugin's settings do, and why you might want to modify their output.</p></div>
	</div>

	<div class="row row-setting row-alpha">
		<div class="col-sm-3 col-lg-2 col-name">What is wp_nav_menu?</div>
		<div class="col-sm-9 col-lg-7 col-setting">
			<p class="description">
				<a href="https://codex.wordpress.org/Function_Reference/wp_nav_menu" target="_blank">wp_nav_menu</a>
			</p>
		</div>
	</div>

	<div class="row row-setting row-alpha">
		<div class="col-sm-3 col-lg-2 col-name">What is wp_page_menu?</div>
		<div class="col-sm-9 col-lg-7 col-setting">
			<p class="description">
				<a href="https://codex.wordpress.org/Function_Reference/wp_page_menu" target="_blank">wp_page_menu</a>
			</p>
		</div>
	</div>

	<div class="row row-setting row-omega">
		<div class="col-sm-3 col-lg-2 col-name">Workarounds</div>
		<div class="col-sm-9 col-lg-7 col-setting">
			<p class="description">
			There are arguments that you can pass to <code>wp_nav_menu</code> to disable the use of <code>wp_page_menu</code>,
			but if it is disabled, no menu will show. You can work around this too, but using these settings just saves you from all the hassle.
			</p>
		</div>
	</div>

<?php }


function muffin_menus_data_cb() { $options = get_option('muffin_menus'); ?>

	<div class="row row-title">
		<div class="col-sm-10 col-title"><h3>Options Data</h3></div>
	</div>

	<div class="row row-info">
		<div class="col-sm-10 col-info"><p>This section lets you see all your saved data at a glance.</p></div>
	</div>

	<div class="row row-setting row-alpha">
		<div class="col-sm-3 col-lg-2 col-name">Formatted</div>
		<div class="col-sm-9 col-lg-7 col-setting">
		<?php if(issetor($options)) : foreach( $options as $key => $value ) { ?>
			<?php echo $key; ?>:
			<code>
			<?php
				if(!isset($value)) {echo '<span class="note">NULL - not set</span>';}
				elseif(empty($value)) {echo '<span class="note">string(0) - set, but empty</span>';}
				else {echo $value;};
			?>
			</code><br />
		<?php } else : echo 'No options data!'; endif; ?>
		</div>
	</div>

	<div class="row row-setting row-omega">
		<div class="col-sm-3 col-lg-2 col-name">Array Data</div>
		<div class="col-sm-9 col-lg-7 col-setting">
			<pre><?php print_r($options); ?></pre>
		</div>
	</div>

<?php }


/*
current-menu-ancestor	
current-menu-item	
current-menu-parent	
current_page_ancestor	
current_page_item	
current_page_parent	
menu-item	
menu-item-home
menu-item-object-category	
menu-item-object-page	
menu-item-object-tag	
menu-item-type-post_type	
menu-item-type-taxonomy
page_item
*/

?>