=== Menu Classes Manager ===
Contributors: Code Muffin
Donate link: https://www.paypal.com/cgi-bin/webscr?cmd=_donations&business=XFDW8V2SZA48E&lc=QA&item_name=Copyright%20Plugin%20Donation%20%2d%20thank%20you%21%20%3a%29&currency_code=USD&bn=PP%2dDonationsBF%3abtn_donateCC_LG%2egif%3aNonHosted
Tags: menu, menus, class, classes, wp_nav_menu, wp_page_menu, custom menus, core
Requires at least: 4.1
Tested up to: 4.2.2
Stable tag: 1
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Add, remove and edit the core menu classes, and update wp_page_menu classes

== Description ==

Add, remove and edit the core menu classes, and update wp_page_menu classes

== Installation ==

= From the Plugins Directory =

1. Go to Plugins > Add New and search for 'Menu Classes Manager', then click the Install button.
2. After installation, click the Activate link to enable the plugin.

= Manual - from the Dashboard =

1. Download the plugin from wordpress.org, then go to Plugins > Add New, and click Upload Plugin.
2. Select the downloaded plugin, and begin the upload. After the upload has finished the installation will run.
3. When the installation is complete, click the Activate link that appears to enable the plugin.

= Manual - via FTP =

1. Download ths plugin from wordpress.org.
2. Unzip the plugin and copy the folder `menu-classes` to any location on your computer.
3. Upload the folder `copyright-shortcode` via FTP to the directory `/wp-content/plugins/`.
4. Enter the WordPress Dashboard, go to Plugins, look for Copyright Shortcode, and click the Activate link close to it.

== Frequently Asked Questions ==

= How do I use this? =

Go to Settings > Menu Classes.

== Screenshots ==

1. One of the options pages

== Changelog ==

= 1.0 =

* Initial release

== Upgrade Notice ==

= 1.0 =

Initial release