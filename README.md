# WP Menu Classes Manager Plugin

Add, remove and edit the core menu classes, and update `wp_page_menu` classes.

To configure, go to Settings > Menu Classes.

![](plugin-screenshot.png)
